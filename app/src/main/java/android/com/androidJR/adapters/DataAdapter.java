package android.com.androidJR.adapters;

import android.com.androidJR.R;
import android.com.androidJR.detailData;
import android.com.androidJR.models.dataModel;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    List<dataModel> Data;
    Context context;

    public DataAdapter(Context context, List<dataModel> Data)
    {
        this.context = context;
        this.Data = Data;
    }

    @NonNull
    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        Log.d("inflater","View Inflated");
        return new ViewHolder(inflater.inflate(R.layout.data_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapter.ViewHolder holder, int position) {
        dataModel currentData = Data.get(position);
        TextView po = holder.namapo;
        TextView poc = holder.namapic;
        TextView kunjungan = holder.waktuKunjungan;

        po.setText("Nama PO: "+currentData.getNamapo());
        poc.setText("Nama PIC: "+currentData.getNamapic());
        kunjungan.setText("Waktu Kunjungan: "+currentData.getTanggal_kunjungan());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, detailData.class);
                context.startActivity(intent);
            }
        });

        Log.d("adapter","View Created");
    }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CardView card;
        public TextView namapo;
        public TextView namapic;
        public TextView waktuKunjungan;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card_data);
            namapo = itemView.findViewById(R.id.namapotext);
            namapic = itemView.findViewById(R.id.namapictext);
            waktuKunjungan = itemView.findViewById(R.id.waktukunjungantext);
        }
    }
}
