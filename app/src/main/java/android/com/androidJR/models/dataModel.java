package android.com.androidJR.models;

public class dataModel {
    String namapo;
    String namapic;
    String nohp;
    String email;
    String tanggal_kunjungan;

    public dataModel(String namapo, String namapic, String nohp, String email, String tanggal_kunjungan) {
        this.namapo = namapo;
        this.namapic = namapic;
        this.nohp = nohp;
        this.email = email;
        this.tanggal_kunjungan = tanggal_kunjungan;
    }

    public dataModel(String namapo, String namapic, String tanggal_kunjungan) {
        this.namapo = namapo;
        this.namapic = namapic;
        this.tanggal_kunjungan = tanggal_kunjungan;
    }

    public String getNamapo() {
        return namapo;
    }

    public void setNamapo(String namapo) {
        this.namapo = namapo;
    }

    public String getNamapic() {
        return namapic;
    }

    public void setNamapic(String namapic) {
        this.namapic = namapic;
    }

    public String getNohp() {
        return nohp;
    }

    public void setNohp(String nohp) {
        this.nohp = nohp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTanggal_kunjungan() {
        return tanggal_kunjungan;
    }

    public void setTanggal_kunjungan(String tanggal_kunjungan) {
        this.tanggal_kunjungan = tanggal_kunjungan;
    }
}
