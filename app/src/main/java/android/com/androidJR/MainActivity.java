package android.com.androidJR;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    Boolean isLoggedIn = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isLoggedIn) {
            Intent intent = new Intent(this, loginUser.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(this, homeActivity.class);
            startActivity(intent);
            finish();
        }
    }
}