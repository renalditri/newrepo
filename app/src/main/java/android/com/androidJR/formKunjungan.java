package android.com.androidJR;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.FileNotFoundException;

public class formKunjungan extends AppCompatActivity {
    ImageView gambarPO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_kunjungan);
        gambarPO = (ImageView)findViewById(R.id.gambar_ttd_po);
    }

    public void tandaTangan(View view) {
        Intent intent = new Intent(this, signatureView.class);
        if(view == findViewById(R.id.upload_ttd_po)) {
            intent.putExtra("asal","po");
        }else{
            intent.putExtra("asal","pic");
        }
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String fileName = (String)getIntent().getStringExtra("filename");
        if(fileName != null)
        {
            Log.d("image status","image loaded");
            try{
                Bitmap bitmap = BitmapFactory.decodeStream(this
                        .openFileInput(fileName));
                gambarPO.setImageBitmap(bitmap);
            }catch(FileNotFoundException e){
                Toast.makeText(this, "Failed to load file", Toast.LENGTH_SHORT).show();
                Log.d("error image",e.toString());
            }

        }
    }
}