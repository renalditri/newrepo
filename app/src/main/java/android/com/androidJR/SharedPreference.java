package android.com.androidJR;

import android.content.SharedPreferences;
import android.content.Context;

public class SharedPreference {
    static final String KEY_USERNAME = "username";
    static final String KEY_EMAIL = "user_mail";
    static final String KEY_PASSWORD = "user_password";
    static final String KEY_LOGGED_IN = "logged_in";

    public static SharedPreferences getSharedPreferences(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidjr", Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public static void setUsername(Context context, String username)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_USERNAME, username);
        editor.apply();
    }

    public static void setMail(Context context, String mail)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_EMAIL, mail);
        editor.apply();
    }

    public static void setPassword(Context context, String password)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(KEY_PASSWORD, password);
        editor.apply();
    }

    public void setLoggedIn(Context context, Boolean loggedin)
    {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(KEY_LOGGED_IN,loggedin);
        editor.apply();
    }

    public static String getUsername(Context context)
    {
        return getSharedPreferences(context).getString(KEY_USERNAME, "guest");
    }

    public static String getPassword(Context context)
    {
        return getSharedPreferences(context).getString(KEY_PASSWORD, "guest");
    }

    public static String getMail(Context context)
    {
        return getSharedPreferences(context).getString(KEY_EMAIL, "guest@mail.com");
    }
    
    public static Boolean getLoggedIn(Context context)
    {
        return getSharedPreferences(context).getBoolean(KEY_LOGGED_IN, false);
    }

}
