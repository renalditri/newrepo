package android.com.androidJR;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.kyanogen.signatureview.SignatureView;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;

public class signatureView extends AppCompatActivity {
    String asal;
    SignatureView sign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature_view);

        asal = getIntent().getStringExtra("asal");
        sign = findViewById(R.id.signature_view);
    }

    public void Submit(View view) {
        Bitmap bitmap = sign.getSignatureBitmap();
        String fileName = createImageFromBitmap(bitmap);
        Intent intent = new Intent(this, formKunjungan.class);
        intent.putExtra("filename",fileName);
        startActivity(intent);
        finish();
    }

    public void Clear(View view) {
        sign.clearCanvas();
    }

    public String createImageFromBitmap(Bitmap bitmap) {
        String fileName = "myImage";//no .png or .jpg needed
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            FileOutputStream fo = openFileOutput(fileName, Context.MODE_PRIVATE);
            fo.write(bytes.toByteArray());
            // remember close file output
            fo.close();
        } catch (Exception e) {
            e.printStackTrace();
            fileName = null;
        }
        return fileName;
    }
}