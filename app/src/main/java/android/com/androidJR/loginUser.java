package android.com.androidJR;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class loginUser extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_user);
    }

    public void registerAkun(View view) {
        Intent intent = new Intent(this, register.class);
        startActivity(intent);
    }

    public void lupaPassword(View view) {
        Intent intent = new Intent(this, lupaPassword.class);
        startActivity(intent);
    }

    public void login(View view) {
        Intent intent = new Intent(this, homeActivity.class);
        startActivity(intent);
        finish();
    }
}