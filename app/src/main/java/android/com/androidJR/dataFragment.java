package android.com.androidJR;

import android.com.androidJR.adapters.DataAdapter;
import android.com.androidJR.models.dataModel;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class dataFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<dataModel> dataList;
    DataAdapter dataAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.data_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dataList = new ArrayList<>();
        recyclerView = view.findViewById(R.id.data_list);

        for(int i = 0; i < 6; i++)
        {
            dataList.add(new dataModel("Agus Satu","Agus Dua",(i+1)+" Agustus 2020"));
            Log.d("add list","Data added to the list");
        }
        this.dataAdapter = new DataAdapter(getContext(),dataList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(this.dataAdapter);
    }
}
