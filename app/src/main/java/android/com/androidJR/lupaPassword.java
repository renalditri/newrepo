package android.com.androidJR;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class lupaPassword extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lupa_password);
    }

    public void konfirmasiEmail(View view) {
        Intent intent = new Intent(this, resetPassword.class);
        startActivity(intent);
    }
}